package br.com.itau.investimentoproduto.controllers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimentoproduto.models.Produto;
import br.com.itau.investimentoproduto.services.ProdutoService;

@RestController
@RequestMapping("/")
public class ProdutoController {
	@Autowired
	ProdutoService produtoService;
	
	Logger logger = LoggerFactory.getLogger(ProdutoController.class);
	
	@GetMapping
	public Iterable<Produto> listar(){
		return produtoService.listar();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity buscar(@PathVariable int id) {
		logger.info("Request recebida!");
		Optional<Produto> produtoOptional = produtoService.buscar(id);
		
		if(produtoOptional.isPresent()) {
			return ResponseEntity.ok(produtoOptional.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	

}
